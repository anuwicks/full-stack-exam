import React, { useEffect, useMemo, useState } from "react";
import moment from "moment";
import { GoogleMap, OverlayView, useLoadScript } from "@react-google-maps/api";
import "./app.css";
import MapMarkerPin from "./components/MapMarkerPin";
import Button from "./components/Button";

const DEFAULT_CENTER = {
  lat: -32.9269165,
  lng: 151.7607144,
};

const App = () => {
  const [trips, setTrips] = useState({});
  const [selectedDate, setSelectedDate] = useState("");
  const [mapInstance, setMapInstance] = useState(null);
  const [tripDates, setTripDates] = useState([]);
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: "AIzaSyB-_YmaltKlfQ05sqZq08SelqWJ_YgHH74",
  });

  const onMapLoad = (map) => {
    setMapInstance(map);
    map.setOptions({ maxZoom: 17 });
  };

  useEffect(() => {
    fetch("/api/getTrips")
      .then((res) => res.json())
      .then((res) => {
        let allTrips = res.trips;
        let tripDates = [];
        let trips = {};
        //console.log(allTrips);

        //For loop that iterates through all trips and adds all trip dates to the tripDates array, this array will contain duplicate values.
        allTrips.forEach((trip) => {
          let date = new Date(trip.startTime);
          tripDates.push(date);
          let formattedDate = moment(date).format("DDD MMM YYYY");
          /* Add trip information as a nested object to the trips object using the formatted trip date as the key; if the formattedDate key already exists, the stops of the trip are added to the existing stops.
          The trip count is increased by 1. */
          if (formattedDate in trips) {
            trips[formattedDate].stops = trips[formattedDate].stops.concat(
              trip.stops
            );
            trips[formattedDate].count++;
          } else {
            //if the formattedDate key does not exist initialise an new object with the trip stops and a trip counter.
            trips[formattedDate] = { stops: trip.stops, count: 1 };
          }
        });
        setTrips(trips);

        //Sort the tripDates array so trips appear in ascending order
        tripDates.sort((a, b) => {
          return a - b;
        });

        // Remove duplicate dates from the tripDates array
        let tripsFormatted = [
          ...new Set(
            tripDates.map((date) => moment(date).format("DDD MMM YYYY"))
          ),
        ];

        setTripDates(tripsFormatted);

        //The first date in the array is selected as default when the app starts
        setSelectedDate(tripsFormatted[0]);
      });
  }, []);

  //Effect to ensure map is always bounded to the trip stop markers, runs every time a new date is selected, the map instance changes or when new trips are added.
  useEffect(() => {
    if (trips[selectedDate] && mapInstance) {
      const bounds = new google.maps.LatLngBounds();
      trips[selectedDate].stops.forEach((stop) =>
        bounds.extend({
          lat: stop.address.latitude,
          lng: stop.address.longitude,
        })
      );
      mapInstance.fitBounds(bounds);
    }

    //Sets document title as required.
    document.title = `${selectedDate} - (${trips[selectedDate]?.count})`;
  }, [selectedDate, mapInstance, trips]);

  //console.log(trips);
  return (
    <>
      <div class='selected-date-title'>
        Selected Trip Date - {moment(selectedDate).format("dddd, MMMM Do YYYY")}
      </div>
      <div class='number-of-trips-title'>
        Number of Trips : ({trips[selectedDate]?.count})
      </div>
      <div class='container'>
        <div class='grid-cell-map'>
          {isLoaded && (
            <GoogleMap
              center={DEFAULT_CENTER}
              onLoad={onMapLoad}
              mapContainerStyle={{
                height: "400px",
                width: "800px",
                border: "1px solid #b2b2b2",
              }}
              zoom={16}
            >
              {trips[selectedDate]?.stops.map((stop) => {
                return (
                  <OverlayView
                    key={`${selectedDate} - ${stop.id} - ${stop.type}`}
                    position={{
                      lat: stop.address.latitude,
                      lng: stop.address.longitude,
                    }}
                    mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
                  >
                    <MapMarkerPin type={stop.type} />
                  </OverlayView>
                );
              })}
            </GoogleMap>
          )}
          <div class='map-title'>
            <h2>Trip Stops </h2>
          </div>
        </div>
        <div class='grid-cell-buttons'>
          <div class='buttons-title'>
            <h3>Trip Dates </h3>
          </div>
          {tripDates.map((date) => (
            <div>
              <Button
                selected={selectedDate}
                date={date}
                onClick={() => {
                  setSelectedDate(date);
                }}
              >
                {date}
              </Button>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default App;
