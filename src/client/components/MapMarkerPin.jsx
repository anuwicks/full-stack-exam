import React from "react";
import styled from "styled-components";

const StyledMarker = styled.div`
  height: 20px;
  width: 20px;
  background-color: white;
  transform: ${(props) =>
    props.type === "departure" ? "rotate(-1deg)" : "translate(4px, 4px)"};
  display: flex;
  border-radius: 50%;
  border: 4px solid
    ${(props) => (props.type === "departure" ? "#36b9d5" : "#42c782")};
  margin-top: -24px;
  justify-content: center;
  display: flex;
  align-items: baseline;
  &:before {
    content: "";
    width: 4px;
    z-index: 1000;
    position: absolute;
    margin-top: 23px;
    height: 4px;
    background: ${(props) =>
      props.type === "departure" ? "#36b9d5" : "#42c782"};
    filter: brightness(75%);
  }
  &:after {
    content: "";
    position: absolute;
    width: 4px;
    margin-top: 24px;
    height: 24px;
    background: ${(props) =>
      props.type === "departure" ? "#36b9d5" : "#42c782"};
    border-bottom-left-radius: 25%;
    border-bottom-right-radius: 25%;
    &:after {
    }
  }
`;

const MarkerType = styled.div`
  margin-top: 4px;
  font-weight: bold;
  text-transform: uppercase;
  color: black;
`;

const MapMarkerPin = (props) => {
  const { type } = props;
  console.log(type);
  return (
    <StyledMarker type={type}>
      <MarkerType>{type[0]}</MarkerType>
    </StyledMarker>
  );
};

export default MapMarkerPin;
