import React from "react";
import styled from "styled-components";
//reference : https://frontendresource.com/css-buttons/

const Button = styled.button`
  background: #87bc58;
  filter: ${(props) =>
    props.selected === props.date ? " brightness(80%)" : ""};
  color: #fff;
  border-radius: 5px;
  box-shadow: 0 2px 0 #6ea140;
  border: 2px solid transparent;
  box-sizing: border-box;
  cursor: pointer;
  font-size: 1rem;
  font-weight: 700;
  line-height: 1;
  margin: 18px;
  padding: 15px 25px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  outline: none;
  position: relative;
  top: 0;
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.5);
  -webkit-transition: all 0.2s ease-in-out 0s;
  -moz-transition: all 0.2s ease-in-out 0s;
  -ms-transition: all 0.2s ease-in-out 0s;
  transition: all 0.2s ease-in-out 0s;
  &:hover {
    filter: brightness(115%);
  }
  &:active {
    filter: brightness(80%);
  }
`;

const StyledButton = (props) => {
  const { onClick, date, selected } = props;
  return (
    <Button date={date} selected={selected} onClick={onClick}>
      {date}
    </Button>
  );
};

export default StyledButton;
